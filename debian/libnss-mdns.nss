hosts	before=dns,resolve	mdns4_minimal [NOTFOUND=return]
hosts	remove-only	mdns
hosts	remove-only	mdns_minimal
hosts	remove-only	mdns4
hosts	remove-only	mdns4_minimal
hosts	remove-only	mdns6
hosts	remove-only	mdns6_minimal
